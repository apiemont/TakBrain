﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakBrain
{
    /// <summary>
    /// An AI that chooses moves for Tak
    /// </summary>
    public class PlayerAI
    {
        /// <summary>
        /// Create an AI to control the given pieces on the board
        /// </summary>
        /// <param name="ownColor"></param>
        /// <param name="sharedBoard"></param>
        public PlayerAI(TakPiece.PieceColor ownColor, GameState sharedGame)
        {
            MyColor = ownColor;
            TheirColor = ownColor == TakPiece.PieceColor.White ? TakPiece.PieceColor.Black : TakPiece.PieceColor.White;
            Game = sharedGame;
        }

        private TakPiece.PieceColor MyColor { get; set; }
        private TakPiece.PieceColor TheirColor { get; set; }
        private GameState Game { get; set; }
        private Random rnd = new Random();

        /// <summary>
        /// Enumerate all legal moves possible for this player, given the state of the board
        /// </summary>
        /// <param name="player"></param>
        /// <param name="boardState"></param>
        /// <returns></returns>
        private List<TakMove> EnumerateMoves(TakPiece.PieceColor player, TakBoard boardState)
        {
            List<TakMove> moves = new List<TakMove>();

            // first off, we can drop pieces on any open space on the board


            return moves;
        }

        public TakMove ChooseNextMove()
        {
            // for now just generate a random flat placement
            List<System.Windows.Point> empty = new List<System.Windows.Point>();
            for(int i=0; i<Game.Board.Size; i++)
            {
                for(int j=0; j<Game.Board.Size; j++)
                {
                    if (Game.Board[i, j].Count == 0)
                        empty.Add(new System.Windows.Point(i, j));
                }
            }
            System.Windows.Point pt = empty[rnd.Next(empty.Count)];

            return new TakMove(Game.TurnNumber == 0 ? TheirColor : MyColor, (int)pt.X, (int)pt.Y);
        }
    }
}
